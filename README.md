# Custom translator

Made by Tomislav Ignjatov

## Description

This is a simple web application that is meant to serve as a learning companion for new languages. While learning a new language you would enter the new words with their translation in the language you are native and just like that you have your own personal dictionary and you can remind to some of them if you forget a word.

I have developed a web backend application made in Spring with Java and using a Postgres database, while for the front end a mobile application made with Ionic.
