create schema lang;

create table lang.languages(
    id bigserial primary key ,
    original_language text,
    translated_language text
);

alter table word.words add lang bigserial;
