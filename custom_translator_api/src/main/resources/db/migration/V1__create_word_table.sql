create schema language;

create table language.eng(
    id bigserial primary key ,
    original text,
    translated text
);
