package com.anixdrone.custom_translator.Services;

import com.anixdrone.custom_translator.Models.EntryWord;
import com.anixdrone.custom_translator.Models.Word;
import com.anixdrone.custom_translator.Repositories.WordRepository;
import org.springframework.stereotype.Service;


import java.util.List;

@Service
public class WordService {

    private final WordRepository wordRepository;

    public WordService(WordRepository wordRepository) {
        this.wordRepository = wordRepository;
    }


    public List<Word> getAllWords() {
        return wordRepository.findAll();
    }


    public Word getWord(Long id) {
       if (wordRepository.findById(id).isPresent()){
           return wordRepository.findById(id).get();
       }
        return null;
    }

    public void editWord(Long id){
        Word word=getWord(id);
        word.setTranslated("столица");
        wordRepository.save(word);
    }

    public boolean checkWord(Long id) {
        return wordRepository.existsById(id);
    }

    public boolean checkWord(EntryWord entryWord) {
        return wordRepository.existsByOriginalAndTranslatedAndLangId(entryWord.getOriginal(), entryWord.getTranslated(),entryWord.getLangId());
    }

    public void addWord(EntryWord entryWord) {
        if(!checkWord(entryWord)) {
            entryWord.flatWords();
            Word real_deal = new Word(entryWord);
            wordRepository.save(real_deal);
        }
    }

    public List<Word> searchWithOriginal(String original,Long langId) {
        return wordRepository.getWordByOriginalContainingAndLangId(original,langId);
    }

    public List<Word> searchWithTranslated(String translated,Long langId){
        return wordRepository.getWordByTranslatedContainingAndLangId(translated,langId);
    }

    public void deleteWord(Long id) {
        if(checkWord(id))
            wordRepository.deleteById(id);
    }
}
