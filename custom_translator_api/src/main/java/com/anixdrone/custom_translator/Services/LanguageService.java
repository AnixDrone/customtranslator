package com.anixdrone.custom_translator.Services;

import com.anixdrone.custom_translator.Models.Language;
import com.anixdrone.custom_translator.Models.LanguageRequest;
import com.anixdrone.custom_translator.Repositories.LanguageRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LanguageService {

    private final LanguageRepository repository;

    public LanguageService(LanguageRepository languageRepository) {
        this.repository = languageRepository;
    }

    public void addLanguage(LanguageRequest request) {
        if (!repository.existsByOriginalLanguageAndTranslatedLanguage(request.getOriginalLanguage(),request.getTranslatedLanguage())
        &&!repository.existsByOriginalLanguageAndTranslatedLanguage(request.getTranslatedLanguage(),request.getOriginalLanguage())){
            repository.save(new Language(request));
        }
    }

    public List<Language> getAllLanguages(){
        return repository.findAll();
    }

    public Language getLanguageById(Long id){
        if (repository.findById(id).isPresent()){
            return repository.findById(id).get();
        }
        return null;
    }
}
