package com.anixdrone.custom_translator.Models;

public class LanguageRequest {
    String originalLanguage;
    String translatedLanguage;

    public LanguageRequest(String originalLanguage, String translatedLanguage) {
        this.originalLanguage = originalLanguage;
        this.translatedLanguage = translatedLanguage;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getTranslatedLanguage() {
        return translatedLanguage;
    }

    public void setTranslatedLanguage(String translatedLanguage) {
        this.translatedLanguage = translatedLanguage;
    }
}
