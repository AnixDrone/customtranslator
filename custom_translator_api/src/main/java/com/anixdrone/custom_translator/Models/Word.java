package com.anixdrone.custom_translator.Models;


import javax.persistence.*;

@Entity
@Table(schema = "word",name = "words")
public class Word {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "original")
    private String original;

    @Column(name = "translated")
    private String translated;

    @Column(name = "lang")
    private Long langId;

    public Word() {
    }

    public Word(String original, String translated, Long langId) {
        this.original = original;
        this.translated = translated;
        this.langId = langId;
    }

    public Long getLangId() {
        return langId;
    }

    public void setLangId(Long langId) {
        this.langId = langId;
    }

    public Word(EntryWord entryWord){
        this.original=entryWord.getOriginal().toLowerCase();
        this.translated=entryWord.getTranslated().toLowerCase();
        this.langId=entryWord.getLangId();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginal() {
        return original;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }
}
