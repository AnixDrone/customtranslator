package com.anixdrone.custom_translator.Models;

public class EntryWord {

    private String original;
    private Long langId;
    private String translated;

    public String getOriginal() {
        return original;
    }

    public void flatWords() {
        this.original = this.original.toLowerCase();
        this.translated = this.translated.toLowerCase();
    }

    public Long getLangId() {
        return langId;
    }

    public void setLangId(Long langId) {
        this.langId = langId;
    }

    public void setOriginal(String original) {
        this.original = original;
    }

    public String getTranslated() {
        return translated;
    }

    public void setTranslated(String translated) {
        this.translated = translated;
    }

    public EntryWord(String original, String translated,Long langId) {
        this.langId=langId;
        this.original = original;
        this.translated = translated;
    }
}
