package com.anixdrone.custom_translator.Models;

import javax.persistence.*;

@Entity
@Table(schema = "lang",name = "languages")
public class Language {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "original_language")
    private String originalLanguage;

    @Column(name = "translated_language")
    private String translatedLanguage;

    public Language(){

    }

    public Long getId() {
        return id;
    }

    public String getOriginalLanguage() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage = originalLanguage;
    }

    public String getTranslatedLanguage() {
        return translatedLanguage;
    }

    public void setTranslatedLanguage(String translatedLanguage) {
        this.translatedLanguage = translatedLanguage;
    }

    public Language(LanguageRequest request){
        this.originalLanguage=request.getOriginalLanguage();
        this.translatedLanguage=request.getTranslatedLanguage();
    }

    public Language( String originalLanguage, String translatedLanguage) {
        this.originalLanguage = originalLanguage;
        this.translatedLanguage = translatedLanguage;
    }

}
