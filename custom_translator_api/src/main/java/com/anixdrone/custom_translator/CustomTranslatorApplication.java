package com.anixdrone.custom_translator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomTranslatorApplication {

    public static void main(String[] args) {
        SpringApplication.run(CustomTranslatorApplication.class, args);
    }

}
