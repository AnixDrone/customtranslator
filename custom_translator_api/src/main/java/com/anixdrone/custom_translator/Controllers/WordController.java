package com.anixdrone.custom_translator.Controllers;

import com.anixdrone.custom_translator.Models.EntryWord;
import com.anixdrone.custom_translator.Models.Word;
import com.anixdrone.custom_translator.Services.WordService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/word/")
public class WordController {

    private final WordService wordService;

    public WordController(WordService wordService) {
        this.wordService = wordService;
    }

    @GetMapping("/all")
    public List<Word> getAll() {
        return wordService.getAllWords();
    }

    @GetMapping("{id}")
    public Word getWord(@PathVariable Long id) {
        return wordService.getWord(id);
    }

    @PostMapping("/add")
    public void addWord(@RequestBody @Valid EntryWord entryWord) {
       wordService.addWord(entryWord);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteWord(@PathVariable Long id) {
      wordService.deleteWord(id);
    }

    @GetMapping("/searchOriginal/{langId}/{original}")
    public List<Word> searchWithOriginal(@PathVariable Long langId,@PathVariable String original) {
        return wordService.searchWithOriginal(original,langId);
    }

    @GetMapping("/searchTranslated/{langId}/{translated}")
    public List<Word> searchWithTranslated(@PathVariable Long langId,@PathVariable String translated) {
        return wordService.searchWithTranslated(translated,langId);
    }

    //ToDo needs refactoring
    @PutMapping("/edit/{id}")
    public void checkWord(@PathVariable Long id){
        wordService.editWord(id);
    }

}
