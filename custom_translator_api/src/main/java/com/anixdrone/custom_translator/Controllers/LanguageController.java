package com.anixdrone.custom_translator.Controllers;

import com.anixdrone.custom_translator.Models.Language;
import com.anixdrone.custom_translator.Models.LanguageRequest;
import com.anixdrone.custom_translator.Services.LanguageService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/lang/")
public class LanguageController {

    private final LanguageService service;

    public LanguageController(LanguageService service) {
        this.service = service;
    }

    @PostMapping
    public void addLanguage(@RequestBody @Valid LanguageRequest request){
            service.addLanguage(request);
    }

    @GetMapping
    public List<Language> getAllLanguages(){
        return service.getAllLanguages();
    }

    @GetMapping("/{id}")
    public Language getLanguageById(@PathVariable Long id){
        return service.getLanguageById(id);
    }

}
