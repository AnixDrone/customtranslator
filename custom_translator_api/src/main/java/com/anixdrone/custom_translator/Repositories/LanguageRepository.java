package com.anixdrone.custom_translator.Repositories;

import com.anixdrone.custom_translator.Models.Language;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LanguageRepository extends JpaRepository<Language,Long> {
    Boolean existsByOriginalLanguageAndTranslatedLanguage(String originalLanguage,String translatedLanguage);
}
