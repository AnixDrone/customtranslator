package com.anixdrone.custom_translator.Repositories;


import com.anixdrone.custom_translator.Models.Word;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;


public interface WordRepository extends JpaRepository<Word, Long> {

    boolean existsByOriginalAndTranslatedAndLangId (String original, String translated,Long langId);

    List<Word> getWordByOriginalContainingAndLangId(String original,Long langId);

    List<Word> getWordByTranslatedContainingAndLangId(String translated,Long langId);
}
