package com.anixdrone.custom_translator.Exceptions;

import java.util.function.Supplier;

public class WordDoesNotExistException extends Exception  {
    public WordDoesNotExistException() {
        super("Word does not exist");
    }
}
