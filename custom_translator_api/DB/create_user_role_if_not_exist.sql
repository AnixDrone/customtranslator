DO
$do$
    BEGIN
        IF NOT EXISTS(SELECT FROM pg_catalog.pg_roles WHERE rolname = 'custom')
        THEN CREATE USER custom WITH SUPERUSER PASSWORD 'custom';
        END IF;
    END
$do$;
